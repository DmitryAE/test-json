//
//  TestJSONTests.swift
//  TestJSONTests
//
//  Created by Dmitry Eryshov on 13/02/16.
//  Copyright © 2016 Dmitry Eryshov. All rights reserved.
//

import XCTest
@testable import TestJSON
//@testable import ObjectMapper
//@testable import SwiftyJSON
//
//struct SimpleObjectFromJSON {
//    
//    struct SimpleOwnerFromJson {
//        
//        let email, name, phone: String
//        let id: Int
//
//        init( dictionary: [String:AnyObject] ) {
//            
//            email = dictionary["email"] as! String
//            name = dictionary["name"] as! String
//            phone = dictionary["phone"] as! String
//            
//            id = dictionary["id"] as! Int
//        }
//    }
//    struct SimpleSeatFromJson {
//        
//        let column: Int
//        let row: String
//        
//        init( dictionary: NSDictionary ) {
//            
//            row = dictionary["row"] as! String
//            column = dictionary["column"] as! Int
//        }
//
//    }
//    
//    let comment, ident, price, status_timestamp: String
//    let id_event_map_zone, id_order, id_event_ticket_type, is_collected: Int
//    let owner: SimpleOwnerFromJson?
//    let seat: SimpleSeatFromJson?
//    
//    init( dictionary: [String: AnyObject] ) {
//        
//        comment = dictionary["comment"] as! String
//        ident = dictionary["ident"] as! String
//        price = dictionary["price"] as! String
//        status_timestamp = String(dictionary["status_timestamp"])
//
//        
//        id_event_map_zone = dictionary["id_event_map_zone"] as! Int
//        id_order = dictionary["id_order"] as! Int
//        id_event_ticket_type = dictionary["id_event_ticket_type"] as! Int
//        is_collected = dictionary["is_collected"] as! Int
//        
//        if let dictOwner = dictionary["owner"] as? [String: AnyObject] {
//         
//            owner = SimpleOwnerFromJson( dictionary: dictOwner )
//        } else {
//            
//            owner = nil
//        }
//        if let dictSeat = dictionary["seat"] as? [String: AnyObject] {
//            
//            seat = SimpleSeatFromJson( dictionary: dictSeat )
//        } else {
//            
//            seat = nil
//        }
//
//    }
//}
//
//struct ObjectMappedFromJSON: Mappable {
//    
//    struct ObjectOwnerFromJson: Mappable {
//        
//        var email, name, phone: String?
//        var id: Int?
//        
//        init?(_ map: Map) {
//            
//        }
//        
//        mutating func mapping(map: Map) {
//            
//            email <- map["email"]
//            name  <- map["name"]
//            phone <- map["phone"]
//            id  <- map["id"]
//
//        }
//    }
//    struct ObjectSeatFromJson: Mappable {
//        
//        var column: Int?
//        var row: String?
//        
//        init?(_ map: Map) {
//            
//        }
//        
//        mutating func mapping(map: Map) {
//
//            column <- map["column"]
//            row  <- map["row"]
//        }
//    }
//    
//    var comment, ident, price, status_timestamp: String?
//    var id_event_map_zone, id_order, id_event_ticket_type, is_collected: Int?
//    var owner: ObjectOwnerFromJson?
//    var seat: ObjectSeatFromJson?
//    
//    init?(_ map: Map) {
//        
//    }
//    
//    mutating func mapping(map: Map) {
//        
//        comment <- map["comment"]
//        ident <- map["ident"]
//        price <- map["price"]
//        status_timestamp <- map["status_timestamp"]
//        
//        id_event_map_zone <- map["id_event_map_zone"]
//        id_order <- map["id_order"]
//        id_event_ticket_type <- map["id_event_ticket_type"]
//        is_collected <- map["is_collected"]
//        
//        owner = Mapper<ObjectOwnerFromJson>().map(map["owner"])
//        seat = Mapper<ObjectSeatFromJson>().map(map["seat"])
//    }
//}
//
//
//
//class TestJSONTests: XCTestCase {
//    
//    var testJSONArray: [[String: AnyObject]]!
//    
//    override func setUp() {
//        super.setUp()
//        
//        if let file = NSBundle.mainBundle().pathForResource("json", ofType: "txt") {
//            
//            do {
//                
//                testJSONArray = try NSJSONSerialization.JSONObjectWithData( NSData(contentsOfFile: file)!, options: .AllowFragments) as! [[String: AnyObject]]
//            } catch {
//                
//                print( error )
//            }
//        } else {
//            XCTFail("Can't find the test JSON file")
//        }
//    }
//    
//    override func tearDown() {
//        // Put teardown code here. This method is called after the invocation of each test method in the class.
//        super.tearDown()
//    }
//    
//    func testSimple() {
//
//        var arrayObjects = [SimpleObjectFromJSON]()
//        self.measureBlock {
//
//            for obj in self.testJSONArray {
//                
//                arrayObjects.append( SimpleObjectFromJSON( dictionary: obj ) )
//            }
//            print( arrayObjects[0] )
//        }
//    }
//
//    func testObjectMapper() {
//        
//        self.measureBlock {
//            
//            let arrayObjects = Mapper<ObjectMappedFromJSON>().mapArray( self.testJSONArray )
//            print( arrayObjects![0] )
//        }
//    }
//
//    func testSwiftyJSON() {
//
//        self.measureBlock {
//            
//        }
//    }
//
//    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measureBlock {
//            // Put the code you want to measure the time of here.
//        }
//    }
//    
//}

