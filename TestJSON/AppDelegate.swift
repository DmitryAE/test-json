//
//  AppDelegate.swift
//  TestJSON
//
//  Created by Dmitry Eryshov on 13/02/16.
//  Copyright © 2016 Dmitry Eryshov. All rights reserved.
//

import UIKit
import ObjectMapper

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var testJSONArray: NSArray!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        let timeStart = Date().timeIntervalSince1970
//        if let file = Bundle.main.path(forResource: "json", ofType: "txt") {
//
//            do {
//
//                testJSONArray = try JSONSerialization.jsonObject( with: Data(contentsOf: URL(fileURLWithPath: file)), options: .allowFragments) as! NSArray
//            } catch {
//
//                print( error )
//            }
//        }
//        simpleTest(startTime: timeStart)

//        simpleSwiftDictionaryTest(startTime: timeStart)

//        objectMapperTest(startTime: timeStart)
        
        codableTest(startTime: timeStart, jsonData: try! Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "json", ofType: "txt")!)))
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    func simpleTest(startTime: TimeInterval) {
        
        var arrayObjects = [SimpleObjectFromJSON]()
        
        for obj in self.testJSONArray {
            
            arrayObjects.append( SimpleObjectFromJSON(dictionary: obj as! NSDictionary) )
        }
        print( arrayObjects[arrayObjects.count - 1] )
        print( Date().timeIntervalSince1970 - startTime )
    }
    
    func simpleSwiftDictionaryTest(startTime: TimeInterval) {
        
        var arrayObjects = [SimpleObjectSwiftDictionaryFromJSON]()
        
        for obj in self.testJSONArray {
            
            arrayObjects.append( SimpleObjectSwiftDictionaryFromJSON(dictionary: obj as! [String: Any]) )
        }
        print( arrayObjects[arrayObjects.count - 1] )
        print( Date().timeIntervalSince1970 - startTime )
    }
    
    
    func objectMapperTest(startTime: TimeInterval) {
        
        let arrayObjects = Mapper<ObjectMappedFromJSON>().mapArray(JSONArray: self.testJSONArray as! [[String : Any]] )
        print( arrayObjects[arrayObjects.count - 1] )
        print( Date().timeIntervalSince1970 - startTime )
    }
    
    func codableTest(startTime: TimeInterval, jsonData: Data) {
        
        let results = try! JSONDecoder().decode([CodableObjectFromJSON].self, from: jsonData)
        
        print( results[results.count - 1] )
        print( Date().timeIntervalSince1970 - startTime )
    }
    
}

struct SimpleObjectFromJSON {
    
    struct SimpleOwnerFromJson {
        
        let email, name, phone: String
        let id: Int
        
        init( dictionary: NSDictionary ) {
            
            email = dictionary["email"] as! String
            name = dictionary["name"] as! String
            phone = dictionary["phone"] as! String
            
            id = dictionary["id"] as! Int
        }
    }
    struct SimpleSeatFromJson {
        
        let column: Int
        let row: String
        
        init( dictionary: NSDictionary ) {
            
            row = dictionary["row"] as! String
            column = dictionary["column"] as! Int
        }
        
    }
    
    let comment, ident, price: String
    let id_event_map_zone, id_order, id_event_ticket_type, is_collected: Int
    let status_timestamp: Int64
    let owner: SimpleOwnerFromJson?
    let seat: SimpleSeatFromJson?
    
    init( dictionary: NSDictionary ) {
        
        comment = dictionary["comment"] as! String
        ident = dictionary["ident"] as! String
        price = dictionary["price"] as! String
        
        
        id_event_map_zone = dictionary["id_event_map_zone"] as! Int
        id_order = dictionary["id_order"] as! Int
        id_event_ticket_type = dictionary["id_event_ticket_type"] as! Int
        is_collected = dictionary["is_collected"] as! Int
        status_timestamp = dictionary["status_timestamp"] as? Int64 ?? 0
        
        if let dictOwner = dictionary["owner"] as? NSDictionary {
            
            owner = SimpleOwnerFromJson( dictionary: dictOwner )
        } else {
            
            owner = nil
        }
        if let dictSeat = dictionary["seat"] as? NSDictionary {
            
            seat = SimpleSeatFromJson( dictionary: dictSeat )
        } else {
            
            seat = nil
        }
        
    }
}

struct SimpleObjectSwiftDictionaryFromJSON {
    
    struct SimpleOwnerFromJson {
        
        let email, name, phone: String
        let id: Int
        
        init( dictionary: [String: Any] ) {
            
            email = dictionary["email"] as! String
            name = dictionary["name"] as! String
            phone = dictionary["phone"] as! String
            
            id = dictionary["id"] as! Int
        }
    }
    struct SimpleSeatFromJson {
        
        let column: Int
        let row: String
        
        init( dictionary: [String: Any] ) {
            
            row = dictionary["row"] as! String
            column = dictionary["column"] as! Int
        }
        
    }
    
    let comment, ident, price: String
    let id_event_map_zone, id_order, id_event_ticket_type, is_collected: Int
    let status_timestamp: Int64
    let owner: SimpleOwnerFromJson?
    let seat: SimpleSeatFromJson?
    
    init( dictionary: [String: Any] ) {
        
        comment = dictionary["comment"] as! String
        ident = dictionary["ident"] as! String
        price = dictionary["price"] as! String
        
        id_event_map_zone = dictionary["id_event_map_zone"] as! Int
        id_order = dictionary["id_order"] as! Int
        id_event_ticket_type = dictionary["id_event_ticket_type"] as! Int
        is_collected = dictionary["is_collected"] as! Int
        status_timestamp = dictionary["status_timestamp"] as? Int64 ?? 0
        
        if let dictOwner = dictionary["owner"] as? [String: Any] {
            
            owner = SimpleOwnerFromJson( dictionary: dictOwner )
        } else {
            
            owner = nil
        }
        if let dictSeat = dictionary["seat"] as? [String: Any] {
            
            seat = SimpleSeatFromJson( dictionary: dictSeat )
        } else {
            
            seat = nil
        }
        
    }
}


struct ObjectMappedFromJSON: Mappable {
    
    struct ObjectOwnerFromJson: ImmutableMappable {
        
        let email, name, phone: String
        let id: Int
        
        init(map: Map) throws {
            email = try map.value("email")
            name = try map.value("name")
            phone = try map.value("phone")
            
            id = try map.value("id")
        }
    }
    struct ObjectSeatFromJson: ImmutableMappable {
        
        let column: Int
        let row: String
        
        init(map: Map) throws {
            column = try map.value("column")
            row = try map.value("row")
        }
    }
    
    var comment = "", ident = "", price = ""
    var id_event_map_zone = 0, id_order = 0, id_event_ticket_type = 0, is_collected = 0
    var status_timestamp: Int64 = 0
    var owner: ObjectOwnerFromJson?
    var seat: ObjectSeatFromJson?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        comment <- map["comment"]
        ident <- map["ident"]
        price <- map["price"]
        status_timestamp <- map["status_timestamp"]
        
        id_event_map_zone <- map["id_event_map_zone"]
        id_order <- map["id_order"]
        id_event_ticket_type <- map["id_event_ticket_type"]
        is_collected <- map["is_collected"]
        
        owner <- map["owner"]
        seat <- map["seat"]
    }
}

struct CodableObjectFromJSON: Codable {
    
    struct CodableOwnerFromJson: Codable {
        
        let email, name, phone: String
        let id: Int
    }
    struct CodableSeatFromJson: Codable {
        
        let column: Int
        let row: String
    }
    
    let comment, ident, price: String
    let id_event_map_zone, id_order, id_event_ticket_type, is_collected: Int
    let status_timestamp: Int64?
    let owner: CodableOwnerFromJson?
    let seat: CodableSeatFromJson?
    
//    init( dictionary: NSDictionary ) {
//
//        comment = dictionary["comment"] as! String
//        ident = dictionary["ident"] as! String
//        price = dictionary["price"] as! String
//
//
//        id_event_map_zone = dictionary["id_event_map_zone"] as! Int
//        id_order = dictionary["id_order"] as! Int
//        id_event_ticket_type = dictionary["id_event_ticket_type"] as! Int
//        is_collected = dictionary["is_collected"] as! Int
//        status_timestamp = dictionary["status_timestamp"] as? Int64 ?? 0
//
//        if let dictOwner = dictionary["owner"] as? NSDictionary {
//
//            owner = SimpleOwnerFromJson( dictionary: dictOwner )
//        } else {
//
//            owner = nil
//        }
//        if let dictSeat = dictionary["seat"] as? NSDictionary {
//
//            seat = SimpleSeatFromJson( dictionary: dictSeat )
//        } else {
//
//            seat = nil
//        }
//
//    }
}

